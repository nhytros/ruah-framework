<?php
session_start();
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', getcwd());

$system_path = 'core';
$application_folder = 'app';
$view_folder = '';

// Set the current directory correctly for CLI requests
if (defined('STDIN')) {chdir(dirname(__FILE__));}
if (($_temp = realpath($system_path)) !== false) {
	$system_path = $_temp.DS;
} else {
	// Ensure there's a trailing slash
	$system_path = strtr(rtrim($system_path,'/\\'),'/\\',DS.DS).DS;
}

// Is the system path correct?
if (!is_dir($system_path)) {
	header('HTTP/1.1 503 Service Unavailable.',true,503);
	die('Your core folder path does not appear to be set correctly. Please open the file <strong>'
		.pathinfo(__FILE__, PATHINFO_BASENAME)
		.'</strong> and correct this.');
}

define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
define('CORE', $system_path);
define('FCPATH', dirname(__FILE__).DIRECTORY_SEPARATOR);
define('SYSDIR', basename(CORE));

if (is_dir($application_folder)){
	if (($_temp = realpath($application_folder)) !== false) {
		$application_folder = $_temp;
	} else {
		$application_folder = strtr(rtrim($application_folder,'/\\'),'/\\',DS.DS);
	}
} elseif (is_dir(CORE.$application_folder.DS)) {
	$application_folder = CORE.strtr(trim($application_folder,'/\\'),'/\\',DS.DS);
} else {
	header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
	die('Your application folder path does not appear to be set correctly. Please open the file <strong>'.SELF.'</strong> and correct this.');
}
define('APP', $application_folder.DS);

// The path to the "views" directory
if (!isset($view_folder[0]) && is_dir(APP.'views'.DS)) {
	$view_folder = APP.'views';
} elseif (is_dir($view_folder)) {
	if (($_temp = realpath($view_folder)) !== FALSE) {
		$view_folder = $_temp;
	} else {
		$view_folder = strtr(rtrim($view_folder,'/\\'),'/\\',DS.DS);
	}
} elseif (is_dir(APP.$view_folder.DS)) {
	$view_folder = APP.strtr(trim($view_folder,'/\\'),'/\\',DS.DS);
} else {
	header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
	die('Your view folder path does not appear to be set correctly. Please open the file <strong>'.SELF.'</strong> and correct this.');
}
define('VIEWS', $view_folder.DS);
define('LAYOUTS', VIEWS.'layouts'.DS);
define('CONFIG', APP.'config'.DS);
define('PUBLIC_PATH', ROOT.DS.'public'.DS);
define('ASSETS', PUBLIC_PATH.'assets'.DS);

require_once CORE.'init.php';
