<?php namespace Migrations;
defined('CORE') OR exit('No direct script access allowed');
use Core\Libs\Migration;

class Migration1550179859 extends Migration {
	public function up() {
		$table = "migrations";
		$this->createTable($table);
		$this->addColumn($table, 'migration', 'varchar',['size'=>35]);
		$this->addIndex($table,'migration');
	}
}
