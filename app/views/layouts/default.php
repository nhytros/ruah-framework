<!DOCTYPE html>
<html lang="<?=Config::get('lang');?>" dir="<?=Config::get('dir');?>">
<head>
	<?=$this->template('head_meta'); ?>
	<?=$this->template('head_links'); ?>
	<title><?=$this->pageTitle();?></title>
	<?=$this->content('head');?>
</head>
<body>
	<div id="main">
		<?php include 'nav_user.php'; ?>
		<?php include 'left_sidebar.php'; ?>
		<div class="content-page">
			<!-- Start content -->
			<div class="content">
				<div class="container-fluid">
					<?php if(Config::get('breadcrumbs')) : ?>
						<?=$this->template('breadcrumbs');?>
					<?php endif; ?>
					<div class="row">
						<div class="col-xl-12">
							<?=Session::displayMessage();?>
							<?=$this->content('body');?>
						</div>
					</div>
				</div> <!-- END container-fluid -->
			</div> <!-- END content -->
		</div> <!-- END content-page -->
		<?php
		/*

		<div class="col-2">
		<?php $this->insert('layouts/cards_right'); ?>
		</div>
		*/
		?>
		<?php $this->insert('layouts/footer');?>
