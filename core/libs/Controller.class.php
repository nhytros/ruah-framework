<?php defined('CORE') OR exit('No direct script access allowed');

class Controller extends App {
	protected $_controller, $_action;
	public $view, $request;

	public function __construct($controller, $action) {
		parent::__construct();
		$this->_controller = $controller;
		$this->_action = $action;
		$this->request = new Input();
		$this->view = new View();
	}

	protected function load_model($model) {
		if(is_array($model)) {
			foreach($model as $m) { self::load($m); }
		} else { self::load($model); }
	}

	protected function load($model) {
		if (class_exists($model)) {
			$this->{$model.'Model'} = new $model(strtolower($model));
		}
	}
}
