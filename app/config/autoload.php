<?php defined('CORE') OR exit('No direct script access allowed');
$autoload['helpers'] = ['mwd','url','common'];

// Autoload Classes
function autoload($class) {
	if(strtolower($class) == 'cc') $class = 'Bank';
	if(file_exists(APP.'custom'.DS.'libs'.DS.$class.'.class.php')) {
		require_once APP.'custom'.DS.'libs'.DS.$class.'.class.php';
	} elseif(file_exists(CORE.'libs'.DS.$class.'.class.php')) {
		require_once CORE.'libs'.DS.$class.'.class.php';
	} elseif(file_exists(APP.'controllers'.DS.$class.'.php')) {
		require_once APP.'controllers'.DS.$class.'.php';
	} elseif(file_exists(APP.'models'.DS.$class.'.php')) {
		require_once APP.'models'.DS.$class.'.php';
	} elseif(file_exists(APP.'custom'.DS.'validators'.DS.$class.'.class.php')) {
		require_once APP.'custom'.DS.'validators'.DS.$class.'.class.php';
	} elseif(file_exists(CORE.'validators'.DS.$class.'.class.php')) {
		require_once CORE.'validators'.DS.$class.'.class.php';
	}
}
spl_autoload_register('autoload');

// Autoload Helpers
foreach($autoload as $a => $val) {
	if (strtolower($a) == 'helpers') {
		foreach($val as $file) {
			if (file_exists(APP.'custom'.DS.$a.DS.$file.'.php')) {
				require_once APP.'custom'.DS.$a.DS.$file.'.php';
			} elseif (file_exists(CORE.$a.DS.$file.'.php')) {
				require_once CORE.$a.DS.$file.'.php';
			}
		}
	}
}

// function autoload($className) {
// 	$clsArray = explode('\\', $className);
// 	vd($clsArray);
// 	$ext = ($clsArray[0] == 'Core' && $clsArray[1] == 'Libs') ? '.class.php' : '.php';
// 	$class = array_pop($clsArray);
// 	$subPath = strtolower(implode(DS,$clsArray));
// 	vd(ROOT.DS.$subPath.DS.$class.$ext);
// 	$path = ROOT.DS.$subPath.DS.$class.$ext;
// 	if(file_exists($path)) {
// 		require_once $path;
// 	}
// }
// spl_autoload_register('autoload');

// Autoload settings.file
if(file_exists(CONFIG.'settings.php')) {
	require_once CONFIG.'settings.php';
}

// Composer autoloader
if (Config::get('composer_vendor') && file_exists(APP.'vendor'.DS.'autoload.php')) {
	require_once APP.'vendor'.DS.'autoload.php';
}
