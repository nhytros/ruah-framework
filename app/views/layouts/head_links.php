<!-- CSS  -->
<link rel="stylesheet" href="<?=Router::site_url('/public/assets/css/bootstrap.min.css');?>">
<link rel="stylesheet" href="<?=Router::site_url('/public/assets/css/font-awesome.min.css');?>">
<link rel="stylesheet" href="<?=Router::site_url('/public/assets/css/custom/custom.css');?>">

<!-- JavaScript -->
<script type="text/javascript" src="<?=Router::site_url('/public/assets/js/jquery-3.3.1.min.js');?>"></script>
<script type="text/javascript" src="<?=Router::site_url('/public/assets/js/popper.min.js');?>"></script>
<script type="text/javascript" src="<?=Router::site_url('/public/assets/js/bootstrap.min.js');?>"></script>
