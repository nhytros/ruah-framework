<?php defined('CORE') OR exit('No direct script access allowed');

function config($item) {
	return (Config::get($item) != '') ? Config::get($item) : null;
}
