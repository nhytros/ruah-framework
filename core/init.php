<?php defined('CORE') OR exit('No direct script access allowed');
require_once CONFIG.'autoload.php';

// Route the request
$url = isset($_SERVER['PATH_INFO']) ? explode('/', ltrim($_SERVER['PATH_INFO'], '/')) : [];

if(!Session::exists(Config::get('CurrentUserSessionName')) && Cookie::exists(Config::get('RememberMeCookieName'))) {
	Users::LoginUserFromCookie();
}

$db = DB::getInstance();

Router::route($url);
