<?php defined('CORE') OR exit('No direct script access allowed');

class Session{
	public static function init() { if(!isset($_SESSION)) { session_start(); }}
	public static function exists($key) { return (isset($_SESSION[$key])); }
	public static function get($key) { return (isset($_SESSION[$key])) ? $_SESSION[$key] : null; }
	public static function set($key, $val) { $_SESSION[$key] = $val; }
	public static function regenID() { if(isset($_SESSION)) {session_regenerate_id();}}
	public static function kill($key) { if(self::exists($key)) { unset($_SESSION[$key]); }}
	public static function uagentNoVersion() { return preg_replace('/\/[a-zA-Z0-9.]+/','',$_SERVER['HTTP_USER_AGENT']); }

	/**
	 * Adds a session alert message
	 * @method addMessage
	 * @param string	$type	can be: primary, secondary, success, danger, warning, info, light, dark
	 * @param string	$msg	the message you want to display in the alert
	 */
	public static function addMessage($type,$msg){
		$sessionName = 'alert-'.$type;
		self::set($sessionName,$msg);
	}

	public static function displayMessage() {
		$alerts = ['alert-primary','alert-secondary','alert-success','alert-danger','alert-warning','alert-info','alert-light','alert-dark'];
		$html = '';
		foreach($alerts as $alert) {
			if(self::exists($alert)) {
				$html .= '<div class="alert '.$alert.' alert-dismissible fade show" role="alert">';
				$html .= self::get($alert);
				$html .= '<button type="button" class="close" data-dismiss="alert" aria-label="'.t('close').'">';
				$html .= '<span aria-hidden="true">&times;</span></button>';
				$html .= '</div>';
				self::kill($alert);
			}
		}
		return $html;
	}
}
