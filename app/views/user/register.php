<?php $this->setPageTitle(t('register_title')); ?>
<?php $this->start('body'); ?>
<div class="col-lg-10 col-xl-9 mx-auto">
	<div class="card card-signin flex-row my-5">
		<div class="card-body">
			<h4 class="card-title text-center"><?=t('register_title');?></h4>
			<?=Form::open('',['name'=>'registerForm','class'=>'form-signin']);?>
			<?=Form::displayErrors($this->displayErrors);?>
			<div class="row">
					<?=Form::input('text',t('lbl_username'),'username',$this->register->username,['class'=>'form-control','tab-stop'=>1,'autofocus'],['class'=>'form-group col-6']);?>
					<?=Form::input('text',t('lbl_email'),'email',$this->register->email,['class'=>'form-control','tab-stop'=>2],['class'=>'form-group col-6']);?>
			</div>
			<div class="row">
					<?=Form::input('text',t('lbl_firstname'),'email',$this->register->first_name,['class'=>'form-control','tab-stop'=>3],['class'=>'form-group col-6']);?>
					<?=Form::input('text',t('lbl_lastname'),'email',$this->register->last_name,['class'=>'form-control','tab-stop'=>4],['class'=>'form-group col-6']);?>
				<div class="form-group col-4">
					<?php
					$options = [
						'username'		=> t('lbl_username'),
						'first_name'	=> t('lbl_firstname'),
						'last_name'		=> t('lbl_lastname'),
						'full_name_fl'	=> t('lbl_fullname_fl'),
						'full_name_lf'	=> t('lbl_fullname_lf'),
					];
					$display_name = ['username'];
					echo t('lbl_public_name');
					echo Form::dropdown(['name'=>'display_name'],$options,'username',['class'=>'form-control here','tab-stop'=>'5']);
					?>
				</div>
				<div class="form-group col-8">
					<?=Form::input('password',t('lbl_password'),'password',$this->register->password,['class'=>'form-control','tab-stop'=>6]);?>
					<?=Form::input('password',t('lbl_confirm_password'),'cpassword',$this->register->getConfirm(),['class'=>'form-control','tab-stop'=>7]);?>
				</div>

			</div>
			<?=Form::hidden('bio','');?>
			<?=Form::hidden('created_at',time());?>
			<?=Form::hidden('status',1);?>
			<?=Form::hidden('role',2);?>
			<?=Form::hidden('last_login',null);?>
			<?php /* $selected=$this->post['display_name']; ?>
			<select class="form-control here" name="display_name" id="display_name" tab-stop=5>
			<option value="username" <?=($this->post['display_name'] == 'username')?'selected':'';?>><?=t('lbl_username');?></option>
			<option value="first_name" <?=($this->post['display_name'] == 'first_name')?'selected':'';?>><?=t('lbl_firstname');?></option>
			<option value="last_name" <?=($this->post['display_name'] == 'last_name')?'selected':'';?>><?=t('lbl_lastname');?></option>
			<option value="full_name_fl" <?=($this->post['display_name'] == 'full_name_fl')?'selected':'';?>><?=t('lbl_fullname_fl');?></option>
			<option value="full_name_lf" <?=($this->post['display_name'] == 'full_name_lf')?'selected':'';?>><?=t('full_name_lf');?></option>
			</select>
			<label for="display_name" class="col-form-label"><?=t('lbl_public_name');?></label>
			</div>
			*/ ?>
			<hr>
			<div class="row col-12">
				<?=Form::button(t('btn_confirm_registration'),'submit',true,['name'=>'register','class'=>'btn btn-lg btn-primary btn-block text-uppercase','tab-stop'=>'8']);?>
			</div>
			<a class="d-block text-center mt-2 small" href="/user/login"><?=t('login');?></a>
			<hr class="my-4">
			<?=Form::close();?>
		</div>
	</div>
</div>
<?php $this->end(); ?>
