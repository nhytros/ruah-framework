<?php defined('CORE') OR exit('No direct script access allowed');

class App {
	public function __construct() {
		$this->_set_reporting();
		$this->_unregister_globals();
	}

	private function _set_reporting() {
		switch (Config::get('mode')) {
			case 'development':
			error_reporting(E_ALL);
			ini_set('display_errors', 1);
			break;
			case 'production':
			ini_set('display_errors', 0);
			ini_set('log_errors', 1);
			ini_set('error_log',LOGSPATH.'errors.log');
			error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
			break;
			default:
			header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
			echo 'The application environment is not set correctly.';
			exit;
		}
	}

	private function _unregister_globals() {
		if (ini_get('register_globals')) {
			$globalsArray = ['_SESSION','_COOKIE','_POST','_GET','_REQUEST','_SERVER','_ENV','_FILES'];
			foreach ($globalsArray as $g) {
				foreach ($GLOBALS[$g] as $key => $val) {
					if ($GLOBALS[$key] === $val) {
						unset($GLOBALS[$key]);
					}
				}
			}
		}
	}
}
