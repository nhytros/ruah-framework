<?php defined('CORE') OR exit('No direct script access allowed');

class UserRegister extends Model {
	public $id, $username, $email, $password, $birth_date,
	$first_name, $last_name, $display_name, $bio=null, $profile_image, $_confirm=false,
	$acl, $last_login=null, $created_at, $status=1, $role=2, $deleted=0;

	public function __construct() {
		parent::__construct('tmp_fake');
	}

	public function validator() {
		// Validate First and Last name
		$this->runValidation(new Required($this,['field'=>'first_name','msg'=>t('error_mandatory_first_name')]));
		$this->runValidation(new Required($this,['field'=>'last_name','msg'=>t('error_mandatory_last_name')]));

		// Validate username
		$this->runValidation(new Required($this,['field'=>'username','msg'=>t('error_mandatory_username')]));
		$this->runValidation(new MinLength($this,['field'=>'username','rule'=>3,'msg'=>t('error_min_username')]));
		$this->runValidation(new MaxLength($this,['field'=>'username','rule'=>30,'msg'=>t('error_max_username')]));
		$this->runValidation(new Unique($this,['field'=>'username','msg'=>t('error_exists_username')]));

		// Validate email
		$this->runValidation(new Required($this,['field'=>'email','msg'=>t('error_mandatory_email')]));
		$this->runValidation(new Email($this,['field'=>'email','msg'=>t('error_invalid_email')]));
		$this->runValidation(new Unique($this,['field'=>'email','msg'=>t('error_exists_email')]));

		// Validate password
		$this->runValidation(new Required($this,['field'=>'password','msg'=>t('error_mandatory_password')]));
		$this->runValidation(new MinLength($this,['field'=>'password','rule'=>8,'msg'=>t('error_min_password')]));
		$this->runValidation(new Match($this,['field'=>'password',$this->_confirm,'msg'=>t('error_not_match_password')]));
	}

	public function setConfirm($value) {
		$this->_confirm = $value;
	}

	public function getConfirm() {
		return $this->_confirm;
	}

	public function getFullName($name) {
		$first_name = $name[0];
		$last_name = $name[1];
	}

	public function beforeSave() {
		$this->created_at = date('U');
		$this->password = Hash::password_hash($this->password, PASSWORD_DEFAULT);
	}

	public function afterSave() {}


}
