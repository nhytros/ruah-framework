<?php defined('CORE') OR exit('No direct script access allowed');

class Form {
	private $tag, $xhtml;

	private function __construct() {}

	public static function open($action='',$attributes=[],$hidden=[]) {
		if(!$action) {
			$action = site_url($_SERVER['PHP_SELF']);
		} elseif(strpos($action,'://') === false) {
			$action = site_url($action);
		}
		$attributes = self::AttrsToString($attributes);
		if(stripos($attributes, 'method') === false) {
			$attributes .= ' method="post"';
		}
		if(stripos($attributes,'accept-charset') === false) {
			$attributes .= ' accept-charset="'.strtolower(Config::get('charset')).'"';
		}
		$form = '<form action="'.$action.'"'.$attributes.">\n";
		if(is_array($hidden)) {
			foreach($hidden as $name => $value) {
				$form .= '<input type="hidden" name="'.$name.'" value="'.Input::sanitize($value).'" />'."\n";
			}
			if(Config::get('csrf_protection') === true) {
				$form .= self::csrf();
			}
		}
		return $form;
	}

	public static function close() { return '</form>'; }

	public static function open_multipart($action='',$attributes=[],$hidden=[]) {
		if(is_string($attributes)) {
			$attributes .= ' enctype="multipart/form-data"';
		} else {
			$attributes['enctype'] = 'multipart/form-data';
		}
		return self::open($action,$attributes,$hidden);
	}

	public static function hidden($name, $value='',$recursing=false) {
		static $form;
		if($recursing === false) { $form = "\n"; }
		if(is_array($name)) {
			foreach($name as $key => $val) {
				self::hidden($key,$val,true);
			}
			return $form;
		}
		if(!is_array($value)) {
			$form .= '<input type="hidden" name="'.$name.'" value="'.Input::sanitize($value)."\" />\n";
		} else {
			foreach($value as $k => $v) {
				$k = is_int($k)?'':$k;
				self::hidden($name.'['.$k.']',$v,true);
			}
		}
		return $form;
	}

	public static function input($type,$label,$name,$value='',$inputAttrs=[],$divAttrs=[]) {
		$input = self::AttrsToString($inputAttrs);
		$html = '<div'.self::AttrsToString($divAttrs).'>';
		// $html .= '<label for="'.$name.'">'.$label.'</label>';
		$html .= '<input type="'.$type.'" placeholder="'.$label.'" id="'.$name.'" name="'.$name.'" value="'.$value.'" '.$input.' />';
		$html .= "</div>\n";
		return $html;
	}

	public static function password($label,$name,$value='',$inputAttrs=[],$divAttrs=[]) {
		return self::input('password',$label,$name,$value,$inputAttrs,$divAttrs);
	}

	public static function upload($data='',$value='',$extra='') {
		$defaults = ['type'=>'file','name'=>''];
		is_array($data) || $data = ['name'=>$data];
		$data['type'] = 'file';
		return '<input '.self::parseFromAttrs($data,$defaults).self::AttrsToString($extra)." /.\n";
	}

	public static function textarea($label,$name,$value='',$inputAttrs=[],$divAttrs=[]) {
		$input = self::AttrsToString($inputAttrs);
		$html = '<div'.self::AttrsToString($divAttrs).'>';
		$html .= '<textarea placeholder="'.$label.'" id="'.$name.'" name="'.$name.'">'.$value.'</textarea>';
		$html .= "</div>\n";
	}

	public static function multiselect($name='',$options=[],$selected=[],$extra='') {
		$extra = self::AttrsToString($extra);
		if(stripos($extra,'multiple') === false) {
			$extra .= ' multiple="multiple"';
		}
		return self::dropdown($name,$options,$selected,$extra);
	}

	public static function checkbox($data='',$value='',$checked=false,$extra='') {
		$defaults = ['type'=>'checkbox','name'=>(!is_array($data)?$data:''), 'value'=>$value];
		if(is_array($data) && array_key_exists('checked', $data)) {
			$checked = $data['checked'];
			if($checked == false) {
				unset($data['checked']);
			} else {
				$data['checked'] = 'checked';
			}
		}
		if($checked == true) {
			$defaults['checked'] = 'checked';
		} else {
			unset($defaults['checked']);
		}
		return '<input '.self::parseFromAttrs($data,$defaults).self::AttrsToString($extra)." />\n";
	}

	public static function radio($data='',$value='',$checked=false,$extra='') {
		is_array($data) || $data = ['name'=>$data];
		$data['type'] = 'radio';
		return self::checkbox($data,$value,$checked,$extra);
	}

	public static function dropdown($data='',$options=[],$selected=[],$extra='') {
		$defaults = [];
		if(is_array($data)) {
			if(isset($data['selected'])) {
				$selected = $data['selected'];
				unset($data['selected']);
			}
			if(isset($data['options'])) {
				$options = $data['options'];
				unset($data['options']);
			}
		} else {
			$defaults = ['name'=>$data];
		}
		is_array($selected) || $selected = [$selected];
		is_array($options) || $options = [$options];
		if(empty($selected)) {
			if(empty($data)) {
				if(isset($data['name'],$_POST[$data['name']])) {
					$selected=[$_POST[$data['name']]];
				}
			} elseif(isset($_POST[$data])) {
				$selected = [$_POST[$data]];
			}
		}
		if(!in_array('id',$data)) $data['id'] = $data['name'];
		$extra = self::AttrsToString($extra);
		$multiple = (count($selected) > 1 && stripos($extra, 'multiple') === false) ? ' multiple="multiple"' : '';
		$form = '<select '.rtrim(self::parseFromAttrs($data,$defaults)).$extra.$multiple.">\n";
		foreach($options as $key => $val) {
			$key = (string) $key;
			if(is_array($val)) {
				if(empty($val)) {
					continue;
				}
				$form .= '<optgroup label="'.$key."\">\n";
				foreach($val as $ogkey => $ogval) {
					$sel = in_array($ogkey, $selected) ? ' selected="selected"' : '';
					$form .= '<option value="'.Input::sanitize($ogkey).'"'.$sel.'>'
					.(string)$val."</option>\n";
				}
				$form .= "</optgroup>\n";
			} else {
				$form .= '<option value="'.Input::sanitize($key).'"'
				.(in_array($key,$selected) ? ' selected="selected"' : '').'>'
				.(string) $val."</option>\n";
			}
		}
		return $form."</select>\n";
	}

	public static function csrf() {
		return self::hidden('csrf_token',Hash::genToken());
	}

	private static function submit($text, $button=false, $type, $inputAttrs=[], $divAttrs=[]) {
		$input = self::AttrsToString($inputAttrs);
		$html = '<div '.self::AttrsToString($divAttrs).'>';
		if($button) {
			$html .= '<button type="'.$type.'" '.$input.'>'.$text.'</button>';
		} else {
			$html .= '<input type="'.$type.'" value="'.$text.'" '.$input.' />';
		}
		$html .= "\n</div>\n";
		return $html;
	}

	public static function button($text, $type='submit', $button=false, $inputAttrs=[], $divAttrs=[]) {
		return self::submit($text,$button,$type,$inputAttrs,$divAttrs);
	}

	public static function label($text='',$id='',$attrs=[]) {
		$label = '<label';
		if($id != '') $label .= ' for="'.$id.'" '.self::AttrsToString($attrs);
		return $label.'>'.$text.'</label>';
	}

	public static function fieldset($legend, $attrs=[]) {
		$fieldset = '<fieldset '.self::AttrsToString($attrs).">\n";
		if($legend !== '') return $fieldset.'<legend>'.$legend."</legend>\n";
		return $fieldset;

	}

	public static function fieldset_close() {
		return "</fieldset>\n";
	}

	public static function posted_values($post) {
		$clean = [];
		foreach($post as $key => $value) {
			$clean[$key] = Input::sanitize($value);
		}
		return $clean;
	}

	public static function displayErrors($errors) {
		$type = 'danger';
		// $hasErrors = (!empty($errors))?' invalid-feedback':'';
		if(!empty($errors)) {
			// Session::flash('danger',(is_array($error)?$error[0]:$error),'');
			$html = '<div class="form-errors alert alert-'.$type.' alert-dismissible fade show" role="alert">';
			// $html .= '<ul class="bg-'.$type.$hasErrors.'">';
			$html .= '<ul>';
			foreach($errors as $field => $error) {
				$html .= '<li class="text-'.$type.'">'.$error.'</li>';
				// $html .= '<script>jQuery("document").ready(function(){jQuery("#'.$field.'").parent().closest("div").addClass("invalid-feedback")})</script>';
			}
			$html .= '</ul></div>';
			return $html;
		}
		return null;
	}

	private static function parseFromAttrs($attributes, $default) {
		if(is_array($attributes)) {
			foreach($default as $key => $val) {
				if(isset($attributes[$key])) {
					$default[$key] = $attributes[$key];
					unset($attributes[$key]);
				}
			}
			if(count($attributes) > 0) {
				$default = array_merge($default,$attributes);
			}
		}
		$att = '';
		foreach($default as $key => $val) {
			if($key === 'value') {
				$val = Input::sanitize($val);
			} elseif($key === 'name' && !strlen($default['name'])) {
				continue;
			}
			$att .= $key.'="'.$val.'" ';
		}
		return $att;
	}

	private static function AttrsToString($attributes, $js=false) {
		$atts = null;
		if(empty($attributes)) { return $atts; }
		if(is_string($attributes)) { return ' '.$attributes; }
		if(is_object($attributes)) { $attributes = (array) $attributes; }
		if(is_array($attributes)) {
			$atts = '';
			foreach($attributes as $key => $val) {
				if(is_numeric($key)) {
					$atts .= ($js) ? $val.'='.$val.',' : ' '.$val.'="'.$val.'"';
				} else {
					$atts .= ($js) ? $key.'='.$val.',' : ' '.$key.'="'.$val.'"';
				}
			}
			return rtrim($atts, ',');
		}
		return null;
	}
}
