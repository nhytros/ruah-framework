<?php namespace Migrations;
defined('CORE') OR exit('No direct script access allowed');

use Core\Libs\Migration;

class MigrationUsers extends Migration {
	public function up() {
		$table = "users";
	    $this->createTable($table);
	    $this->addTimeStamps($table);
	    $this->addColumn($table,'username','varchar',['size'=>30]);
	    $this->addColumn($table,'email','varchar',['size'=>100]);
	    $this->addColumn($table,'password','varchar',['size'=>60]);
	    $this->addColumn($table,'display_name','varchar',['size'=>20]);
	    $this->addColumn($table,'acl','text');
	    $this->addColumn($table,'last_login','integer');
		$this->addColumn($table,'status','tinyint');
		$this->addSoftDelete($table);

	    $table = "user_sessions";
	    $this->createTable($table);
	    $this->addTimeStamps($table);
	    $this->addColumn($table,'uid','int');
	    $this->addColumn($table,'session','varchar',['size'=>255]);
	    $this->addColumn($table,'user_agent','varchar',['size'=>255]);
	    $this->addIndex($table,'uid');
	    $this->addIndex($table,'session');

		$table = "profiles";
		$this->createTable($table);
		$this->addTimeStamps($table);
		$this->addColumn($table,'uid','int');
		$this->addColumn($table,'first_name','varchar',['size'=>30]);
		$this->addColumn($table,'middle_name','varchar',['size'=>30]);
		$this->addColumn($table,'last_name','varchar',['size'=>30]);
		$this->addColumn($table,'bio','text');
		$this->addColumn($table,'gender','varchar',['size'=>1]);
		$this->addColumn($table,'birth_date','integer');
		$this->addColumn($table,'profile_image','varchar',['size'=>255]);
		$this->addColumn($table,'country','varchar',['size'=>100]);
		$this->addColumn($table,'zipcode','varchar',['size'=>6]);
		$this->addColumn($table,'district','varchar',['size'=>100]);
		$this->addColumn($table,'address','varchar',['size'=>100]);
		$this->addColumn($table,'address2','varchar',['size'=>100]);
		$this->addColumn($table,'state','varchar',['size'=>3]);
		$this->addColumn($table,'phone_home','varchar',['size'=>20]);
		$this->addColumn($table,'phone_cell','varchar',['size'=>20]);
		$this->addColumn($table,'phone_work','varchar',['size'=>20]);
		$this->addColumn($table,'donor','varchar',['size'=>5]);
		$this->addColumn($table,'blood_group','varchar',['size'=>5]);
		$this->addSoftDelete($table);
		$this->addIndex($table,'uid');

		$table = "money";
		$this->createTable($table);
		$this->addTimeStamps($table);
		$this->addColumn($table,'uid','int');
		$this->addColumn($table,'aa_cash','bigint');
		$this->addColumn($table,'aa_bank','bigint');
		$this->addColumn($table,'last_activity','int');
		$this->addColumn($table,'pin','varchar',['size'=>60]);
		$this->addColumn($table,'status','tinyint');
		$this->addColumn($table,'open','tinyint');
		$this->addSoftDelete($table);
		$this->addIndex($table,'uid');

		$table = "operations";
		$this->createTable($table);
		$this->addColumn($table,'uid','int');
		$this->addColumn($table,'date','int');
		$this->addColumn($table,'operation','varchar',['size'=>255]);
		$this->addColumn($table,'cash','decimal',['precision'=>10,'scale'=>2]);
		$this->addColumn($table,'bank','decimal',['precision'=>10,'scale'=>2]);		
	}
}
